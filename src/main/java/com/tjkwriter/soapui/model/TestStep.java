package com.tjkwriter.soapui.model;

import java.util.List;

public class TestStep {
    String testCaseName;

    String name;

    List<Assertion> Assertions;

    boolean isSuccess;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Assertion> getAssertions() {
        return Assertions;
    }

    public void setAssertions(List<Assertion> assertions) {
        Assertions = assertions;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public String getTestCaseName() {
        return testCaseName;
    }

    public void setTestCaseName(String testCaseName) {
        this.testCaseName = testCaseName;
    }


    public TestStep(String name, List<Assertion> assertions, boolean isSuccess, String testCaseName) {
        this.name = name;
        this.Assertions = assertions;
        this.isSuccess = isSuccess;
        this.setTestCaseName(testCaseName);
    }

    public TestStep() {
    }
}
