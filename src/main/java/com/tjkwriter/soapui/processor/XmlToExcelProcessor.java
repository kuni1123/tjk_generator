package com.tjkwriter.soapui.processor;

import com.tjkwriter.soapui.model.Assertion;
import com.tjkwriter.soapui.model.TestStep;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class XmlToExcelProcessor {

    private final static String LEPES_SORSZAMA = "Lépés sorszáma";
    private final static String TESZTELT_RENDSZER = "Tesztelt rendszer";
    private final static String LEPES_LEIRASA = "Lépés leírása";
    private final static String VART_EREDMENY = "Várt eredmény";
    private final static String BEKOVETKEZO_EREDMENY = "Bekövetkező eredmény";
    private final static String KAPCSOLODO_HIBA = "Kapcsolódó hiba";
    private final static String HASZNALT_PARAMETEREK = "Használt paraméterek";
    private final static String STATUSZ = "Státusz";
    private final static String MEGJEGYZES = "Megjegyzés";

    private static final short EXCEL_COLUMN_WIDTH_FACTOR = 256;
    private static final short EXCEL_ROW_HEIGHT_FACTOR = 20;
    private static final int UNIT_OFFSET_LENGTH = 7;
    private static final int[] UNIT_OFFSET_MAP = new int[]{0, 36, 73, 109, 146, 182, 219};

    private CellStyle boldFontWhiteBackCenterBig;
    private CellStyle boldFontWhiteBackCenterSmall;
    private CellStyle normalFontWhiteBackCenterSmall;
    private CellStyle boldFont25percentGreyBackCenter;

    private CellStyle boldFontGreyBlueBackCenterAlign;
    private CellStyle greenFontGreenBackCenterAlign;

    private CellStyle italicFontSmall;
    private CellStyle normalFontBig;

    private SimpleDateFormat  dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static XSSFWorkbook workbook = new XSSFWorkbook();

    private void setStylesTAKINFO() {

        //Cím betűtípus
        boldFontWhiteBackCenterBig = workbook.createCellStyle();

        Font boldFontBig = workbook.createFont();
        boldFontBig.setBold(true);
        boldFontBig.setFontHeightInPoints((short) 18);

        boldFontWhiteBackCenterBig.setFont(boldFontBig);
        boldFontWhiteBackCenterBig.setAlignment(CellStyle.ALIGN_CENTER);
        setCellBorder(boldFontWhiteBackCenterBig, BorderStyle.THIN);
        //Alcím betűtípus

        Font boldFontSmall = workbook.createFont();
        boldFontSmall.setBold(true);
        boldFontSmall.setFontHeightInPoints((short) 10);

        boldFontWhiteBackCenterSmall = workbook.createCellStyle();
        boldFontWhiteBackCenterSmall.setFont(boldFontSmall);
        boldFontWhiteBackCenterSmall.setAlignment(CellStyle.ALIGN_CENTER);
        setCellBorder(boldFontWhiteBackCenterSmall, BorderStyle.THIN);

        setDefaultStyles();

        //25%-os szürke hátterű alcím félkövér betűítpussal

        boldFont25percentGreyBackCenter = workbook.createCellStyle();
        boldFont25percentGreyBackCenter.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        boldFont25percentGreyBackCenter.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        boldFont25percentGreyBackCenter.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        boldFont25percentGreyBackCenter.setAlignment(CellStyle.ALIGN_CENTER);
        Font boldFontMedium = workbook.createFont();
        boldFontMedium.setFontHeightInPoints((short) 11);
        boldFontMedium.setBold(true);
        setCellBorder(boldFont25percentGreyBackCenter, BorderStyle.THICK);
    }

    private void setStylesPOTENYR() {

        setDefaultStyles();

        boldFontGreyBlueBackCenterAlign = workbook.createCellStyle();
        Font boldFont = workbook.createFont();
        boldFont.setFontHeightInPoints((short) 13);

        boldFontGreyBlueBackCenterAlign.setFont(boldFont);
        boldFontGreyBlueBackCenterAlign.setFillForegroundColor(IndexedColors.SEA_GREEN.getIndex());
        boldFontGreyBlueBackCenterAlign.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        boldFontGreyBlueBackCenterAlign.setAlignment(CellStyle.ALIGN_CENTER);

        setCellBorder(boldFontGreyBlueBackCenterAlign, BorderStyle.THIN);

        greenFontGreenBackCenterAlign = workbook.createCellStyle();
        Font greenFont = workbook.createFont();
        greenFont.setFontHeightInPoints((short) 11);
        greenFont.setColor(IndexedColors.BLACK.getIndex());

        greenFontGreenBackCenterAlign.setFont(greenFont);
        greenFontGreenBackCenterAlign.setFillForegroundColor(IndexedColors.BRIGHT_GREEN.getIndex());
        greenFontGreenBackCenterAlign.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        greenFontGreenBackCenterAlign.setAlignment(CellStyle.ALIGN_CENTER);

        setCellBorder(greenFontGreenBackCenterAlign, BorderStyle.THIN);

    }

    private void setDefaultStyles() {
        //Átlagos betűtípus

        normalFontWhiteBackCenterSmall = workbook.createCellStyle();
        Font normalFont = workbook.createFont();
        normalFont.setFontHeightInPoints((short) 10);

        normalFontWhiteBackCenterSmall.setFont(normalFont);
        normalFontWhiteBackCenterSmall.setAlignment(CellStyle.ALIGN_CENTER);
        setCellBorder(normalFontWhiteBackCenterSmall, BorderStyle.THIN);
    }

    private void setStylesEESZT() {
        boldFontWhiteBackCenterBig = workbook.createCellStyle();

        Font boldFontBig = workbook.createFont();
        boldFontBig.setBold(true);
        boldFontBig.setFontHeightInPoints((short) 18);

        boldFontWhiteBackCenterBig.setFont(boldFontBig);
        boldFontWhiteBackCenterBig.setAlignment(CellStyle.ALIGN_CENTER);

        setCellBorder(boldFontWhiteBackCenterBig, BorderStyle.THIN);

        Font boldFontSmall = workbook.createFont();
        boldFontSmall.setBold(true);
        boldFontSmall.setFontHeightInPoints((short) 10);

        boldFontWhiteBackCenterSmall = workbook.createCellStyle();
        boldFontWhiteBackCenterSmall.setFont(boldFontSmall);
        boldFontWhiteBackCenterSmall.setAlignment(CellStyle.ALIGN_CENTER);

        setCellBorder(boldFontWhiteBackCenterSmall, BorderStyle.THICK);

        setDefaultStyles();

        Font italicFont = workbook.createFont();
        italicFont.setItalic(true);
        italicFont.setFontHeightInPoints((short) 10);

        italicFontSmall = workbook.createCellStyle();
        italicFontSmall.setFont(italicFont);

        setCellBorder(italicFontSmall, BorderStyle.THIN);

        Font normalFont = workbook.createFont();
        normalFont.setFontHeightInPoints((short) 11);

        normalFontBig = workbook.createCellStyle();
        normalFontBig.setFont(normalFont);

        setCellBorder(normalFontBig, BorderStyle.THIN);
    }

    private void setCellBorder(CellStyle cellStyle, BorderStyle borderStyle) {
        cellStyle.setBorderBottom(borderStyle);
        cellStyle.setBorderLeft(borderStyle);
        cellStyle.setBorderRight(borderStyle);
        cellStyle.setBorderTop(borderStyle);
    }

    private void prepareTheWorkbookPOTENYR() {

        workbook = new XSSFWorkbook();
        setStylesPOTENYR();
        Sheet sheet = workbook.createSheet("Tesztnapló");
        sheet.setColumnWidth(0, pixel2WidthUnits(100));
        sheet.setColumnWidth(1, pixel2WidthUnits(105));
        sheet.setColumnWidth(2, pixel2WidthUnits(120));
        sheet.setColumnWidth(3, pixel2WidthUnits(100));
        sheet.setColumnWidth(4, pixel2WidthUnits(160));
        sheet.setColumnWidth(5, pixel2WidthUnits(110));
        sheet.setColumnWidth(6, pixel2WidthUnits(160));
        sheet.setColumnWidth(7, pixel2WidthUnits(50));
        sheet.setColumnWidth(8, pixel2WidthUnits(200));

        int rowNum = 0;
        Row row = sheet.createRow(rowNum++);
        Cell cell = row.createCell(0);
        cell.setCellValue(LEPES_SORSZAMA);
        cell.setCellStyle(boldFontGreyBlueBackCenterAlign);

        cell = row.createCell(1);
        cell.setCellValue(TESZTELT_RENDSZER);

        cell.setCellStyle(boldFontGreyBlueBackCenterAlign);


        cell = row.createCell(2);
        cell.setCellValue(LEPES_LEIRASA);
        cell.setCellStyle(boldFontGreyBlueBackCenterAlign);

        cell = row.createCell(3);
        cell.setCellValue(VART_EREDMENY);
        cell.setCellStyle(boldFontGreyBlueBackCenterAlign);

        cell = row.createCell(4);
        cell.setCellValue(BEKOVETKEZO_EREDMENY);
        cell.setCellStyle(boldFontGreyBlueBackCenterAlign);

        cell = row.createCell(5);
        cell.setCellValue(KAPCSOLODO_HIBA);
        cell.setCellStyle(boldFontGreyBlueBackCenterAlign);

        cell = row.createCell(6);
        cell.setCellValue(HASZNALT_PARAMETEREK);
        cell.setCellStyle(boldFontGreyBlueBackCenterAlign);

        cell = row.createCell(7);
        cell.setCellValue(STATUSZ);
        cell.setCellStyle(boldFontGreyBlueBackCenterAlign);

        cell = row.createCell(8);
        cell.setCellValue(MEGJEGYZES);
        cell.setCellStyle(boldFontGreyBlueBackCenterAlign);

    }

    private void prepareTheWorkbookTAKINFO() {
        workbook = new XSSFWorkbook();

        setStylesTAKINFO();
        Sheet sheet = workbook.createSheet("Tesztnapló");

        sheet.setColumnWidth(0, pixel2WidthUnits(207));
        sheet.setColumnWidth(1, pixel2WidthUnits(250));
        sheet.setColumnWidth(2, pixel2WidthUnits(295));
        sheet.setColumnWidth(3, pixel2WidthUnits(488));
        sheet.setColumnWidth(4, pixel2WidthUnits(164));
        sheet.setColumnWidth(5, pixel2WidthUnits(175));
        sheet.setColumnWidth(6, pixel2WidthUnits(147));

        int rowNum = 0;
        Row row = sheet.createRow(rowNum);
        createCell(0, rowNum, rowNum++, row, sheet, "Megrendelői Tesztjegyzőkönyv", boldFontWhiteBackCenterBig, true,6);

        row = sheet.createRow(rowNum);
        createCell(0, rowNum, rowNum, row, sheet, "Az alábbiakban ismertetett „Megrendelői Tesztjegyzőkönyv” sablon formailag  kötelező, minden Tesztjegyzőkönyvben benne kell lennie az itt bemutatott tartalomnak. ", normalFontWhiteBackCenterSmall, true,6);
        rowNum++;

        row = sheet.createRow(rowNum);
        createCell(0, rowNum, rowNum, row, sheet, "TESZTELŐ(K)", boldFont25percentGreyBackCenter, true,6);
        rowNum++;

        row = sheet.createRow(rowNum);
        createCell(0, rowNum, rowNum, row, sheet, "Név:", boldFontWhiteBackCenterSmall, false,0);
        rowNum++;

        row = sheet.createRow(rowNum);
        createCell(0, rowNum, rowNum, row, sheet, "Terület:", boldFontWhiteBackCenterSmall, false,0);
        rowNum++;

        row = sheet.createRow(rowNum);
        createCell(0, rowNum, rowNum, row, sheet, "Dátum:", boldFontWhiteBackCenterSmall, false,0);
        rowNum++;
        rowNum++;

        row = sheet.createRow(rowNum);
        createCell(0, rowNum, rowNum, row, sheet, "TÁRGY", boldFont25percentGreyBackCenter, true,6);
        rowNum++;

        row = sheet.createRow(rowNum);
        createCell(0, rowNum, rowNum, row, sheet, "Hiba/Igény azonosítója:", boldFontWhiteBackCenterSmall, false,0);
        rowNum++;

        row = sheet.createRow(rowNum);
        createCell(0, rowNum, rowNum, row, sheet, "Érintett Rendszer neve:", boldFontWhiteBackCenterSmall, false,0);
        rowNum++;

        row = sheet.createRow(rowNum);
        createCell(0, rowNum, rowNum, row, sheet, "Verziószám", boldFontWhiteBackCenterSmall, false,0);
        rowNum++;
        rowNum++;

        row = sheet.createRow(rowNum);
        createCell(0, rowNum, rowNum, row, sheet, "TESZTELÉSI FELADATOK", boldFont25percentGreyBackCenter, true,6);
        rowNum++;

        row = sheet.createRow(rowNum);
        createCell(0, rowNum, rowNum, row, sheet, "Sorszám", boldFontWhiteBackCenterSmall, false,0);
        createCell(1, rowNum, rowNum, row, sheet, "Hiba/Igény leírása", boldFontWhiteBackCenterSmall, false,1);
        createCell(2, rowNum, rowNum, row, sheet, "Ellenőrzött funkció/Végrehajtott teszt leírása (tesztadat megadásával)", boldFontWhiteBackCenterSmall, false,2);
        createCell(3, rowNum, rowNum, row, sheet, "Elvárt eredmény", boldFontWhiteBackCenterSmall, false,3);
        createCell(4, rowNum, rowNum, row, sheet, "Tesztkörnyezet", boldFontWhiteBackCenterSmall, false,4);
        createCell(5, rowNum, rowNum, row, sheet, "Tesztelés eredménye", boldFontWhiteBackCenterSmall, false,5);
        createCell(6, rowNum, rowNum, row, sheet, "Élesíthető? ", boldFontWhiteBackCenterSmall, false,6);

    }

    private void prepareTheWorkbookEESZT() {
        workbook = new XSSFWorkbook();

        setStylesEESZT();
        Sheet sheet = workbook.createSheet("Tesztnapló");

        sheet.setColumnWidth(0, pixel2WidthUnits(258));
        sheet.setColumnWidth(1, pixel2WidthUnits(364));
        sheet.setColumnWidth(2, pixel2WidthUnits(75));
        sheet.setColumnWidth(3, pixel2WidthUnits(681));
        sheet.setDisplayGridlines(false);
        int rowNum = 0;
        Row row = sheet.createRow(rowNum);
        createCell(0, rowNum, rowNum, row, sheet, "EESZT - Elektronikus Egészségügyi Szolgáltatás Tér", boldFontWhiteBackCenterBig, true,3);

        rowNum++;
        row = sheet.createRow(rowNum);
        createCell(0, rowNum, rowNum, row, sheet, "Tesztelési jegyzőkönyv", boldFontWhiteBackCenterBig, true,3);

        rowNum++;
        row = sheet.createRow(rowNum);
        createRowEESZT(row, rowNum, sheet, "Honlap", "");

        rowNum++;
        row = sheet.createRow(rowNum);
        createRowEESZT(row, rowNum, sheet, "URL", "");

        rowNum++;
        row = sheet.createRow(rowNum);
        createRowEESZT(row, rowNum, sheet, "Böngésző", "SoapUI");

        rowNum++;
        row = sheet.createRow(rowNum);
        createRowEESZT(row, rowNum, sheet, "Felhasználó", "allampolgar");

        rowNum++;
        row = sheet.createRow(rowNum);
        createRowEESZT(row, rowNum, sheet, "Intézmény, szervezeti egység", "-");

        rowNum++;
        row = sheet.createRow(rowNum);
        createRowEESZT(row, rowNum, sheet, "Modul", "KAT");

        rowNum++;
        row = sheet.createRow(rowNum);
        createRowEESZT(row, rowNum, sheet, "Shipment", "");

        rowNum++;
        row = sheet.createRow(rowNum);
        createRowEESZT(row, rowNum, sheet, "Redmine", "");

        rowNum++;
        row = sheet.createRow(rowNum);
        createRowEESZT(row, rowNum, sheet, "Rövid Leírás", "");

        rowNum++;
        row = sheet.createRow(rowNum);
        createRowEESZT(row, rowNum, sheet, "Készítette", "Kun Dávid");

        rowNum++;
        row = sheet.createRow(rowNum);
        createRowEESZT(row, rowNum, sheet, "Dátum", dateFormat.format(new Date()) );

        rowNum++;
        row = sheet.createRow(rowNum);
        createRowEESZT(row, rowNum, sheet, "Becsült idő", "5 perc");

        rowNum++;
        rowNum++;
        row = sheet.createRow(rowNum);
        createCell(0, rowNum, rowNum, row, sheet, "Feladat", boldFontWhiteBackCenterSmall, false, 0);
        createCell(1, rowNum, rowNum, row, sheet, "Elvárt eredmény", boldFontWhiteBackCenterSmall, false, 1);
        createCell(2, rowNum, rowNum, row, sheet, "✔/❌", boldFontWhiteBackCenterSmall, false, 2);
        createCell(3, rowNum, rowNum, row, sheet, "Megjegyzés", boldFontWhiteBackCenterSmall, false, 3);

    }

    private void createRowEESZT(Row row, int rowNum, Sheet sheet, String cim, String szoveg) {
        createCell(0, rowNum, rowNum, row, sheet, cim, italicFontSmall, false, 0);

        createCell(1, rowNum, rowNum, row, sheet, szoveg, normalFontBig, false, 1);
        createCell(2, rowNum, rowNum, row, sheet, "", normalFontBig, false, 2);
        createCell(3, rowNum, rowNum, row, sheet, "", normalFontBig, false, 3);
    }

    private void workbookEndTAKINFO(int rowNum) {
        rowNum++;
        rowNum++;
        Sheet sheet = workbook.getSheetAt(0);
        Row row = sheet.createRow(rowNum);
        createCell(0, rowNum, rowNum, row, sheet, "KIHAGYOTT TESZTESETEK", boldFont25percentGreyBackCenter, true, 6);
        rowNum++;
        row = sheet.createRow(rowNum);
        createCell(0, rowNum, rowNum, row, sheet, "Sorszám", boldFontWhiteBackCenterSmall, false, 0);
        createCell(1, rowNum, rowNum, row, sheet, "Hiba/Igény leírása", boldFontWhiteBackCenterSmall, false, 1);
        createCell(2, rowNum, rowNum, row, sheet, "Indoklás", boldFontWhiteBackCenterSmall, true, 6);
        rowNum++;
        setCellRangeStyle(0, rowNum, rowNum, sheet, normalFontWhiteBackCenterSmall, 0);
        rowNum++;
        setCellRangeStyle(0, rowNum, rowNum, sheet, normalFontWhiteBackCenterSmall, 0);
        rowNum++;
        setCellRangeStyle(0, rowNum, rowNum, sheet, normalFontWhiteBackCenterSmall, 0);
    }

    private void setCellRangeStyle(int startColumn, int mergedRowStart, int mergedRowEnd, Sheet sheet, CellStyle cellStyle, int lastCol) {
        CellRangeAddress cellRangeAddress = new CellRangeAddress(mergedRowStart, mergedRowEnd, startColumn, lastCol);
        RegionUtil.setBorderBottom(cellStyle.getBorderBottom(), cellRangeAddress, sheet, workbook);
        RegionUtil.setBorderTop(cellStyle.getBorderTop(), cellRangeAddress, sheet, workbook);
        RegionUtil.setBorderLeft(cellStyle.getBorderLeft(), cellRangeAddress, sheet, workbook);
        RegionUtil.setBorderRight(cellStyle.getBorderRight(), cellRangeAddress, sheet, workbook);
    }

    private void createCell(int startColumn, int mergedRowStart, int mergedRowEnd, Row row, Sheet sheet, String cellValue, CellStyle cellStyle, boolean withMerge, int lastcol) {
        setCellRangeStyle(startColumn, mergedRowStart, mergedRowEnd, sheet, cellStyle, lastcol);
        if (withMerge) {
            sheet.addMergedRegion(new CellRangeAddress(mergedRowStart, mergedRowEnd, startColumn, lastcol));
        }
        Cell cell = row.createCell(startColumn);
        cell.setCellValue(cellValue);
        cell.setCellStyle(cellStyle);
    }

    public void parseXML(String fileName, String taskNum, String reqID, String projectCode) {
        List<TestStep> testStepList = new ArrayList<>();
        List<Assertion> assertionList = new ArrayList<>();
        TestStep testStep = null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(fileName));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    if (startElement.getName().getLocalPart().equals("testStep")) {
                        testStep = new TestStep();
                        assertionList = new ArrayList<>();
                        //Get the 'id' attribute from Employee element
                        Attribute idAttr = startElement.getAttributeByName(new QName("name"));
                        if (idAttr != null) {
                            testStep.setName(idAttr.getValue());
                        }
                    }
                    //set the other varibles from xml elements
                    else if (startElement.getName().getLocalPart().equals("assertion")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        if (startElement.getAttributeByName(new QName("name")) != null) {
                            assertionList.add(new Assertion(startElement.getAttributeByName(new QName("name")).toString(), startElement.getAttributeByName(new QName("type")).toString()));
                        } else {
                            assertionList.add(new Assertion(startElement.getAttributeByName(new QName("type")).toString().replace("type", "name"), startElement.getAttributeByName(new QName("type")).toString()));
                        }
                    }
                }
                //if Employee end element is reached, add employee object to list
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("testStep")) {
                        testStep.setAssertions(assertionList);
                        testStepList.add(testStep);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        switch (projectCode) {
            case "1":
                prepareTheWorkbookPOTENYR();
                exportToXlsxPOTENYR(testStepList, taskNum, reqID);
                break;
            case "2":
                prepareTheWorkbookEESZT();
                exportToXlsxEESZT(testStepList, taskNum);
                break;
            case "3":
                prepareTheWorkbookTAKINFO();
                exportToXlsxTAKINFO(testStepList, taskNum);
                break;
        }
    }

    private void exportToXlsxTAKINFO(List<TestStep> testStepList, String taskNum) {
        CreationHelper createHelper = workbook.getCreationHelper();

        int stepNum = 1;
        int rowNum = 14;
        Sheet sheet = workbook.getSheetAt(0);
        for (TestStep ts : testStepList) {
            Row row = sheet.createRow(rowNum++);
            createCell(0, rowNum, rowNum, row, sheet, stepNum + "", normalFontWhiteBackCenterSmall, false,0);
            stepNum++;
            createCell(1, rowNum, rowNum, row, sheet, ts.getName(), normalFontWhiteBackCenterSmall, false,1);

            createCell(2, rowNum, rowNum, row, sheet, getAssertionString(ts.getAssertions()), normalFontWhiteBackCenterSmall, false,2);

            createCell(3, rowNum, rowNum, row, sheet, "Sikeres futás", normalFontWhiteBackCenterSmall, false,3);
            createCell(4, rowNum, rowNum, row, sheet, "", normalFontWhiteBackCenterSmall, false,4);
            createCell(5, rowNum, rowNum, row, sheet, "sikeres", normalFontWhiteBackCenterSmall, false,5);
            createCell(6, rowNum, rowNum, row, sheet, "", normalFontWhiteBackCenterSmall, false,6);
        }
        workbookEndTAKINFO(rowNum);
        writeTofile(taskNum);

    }

    private void exportToXlsxEESZT(List<TestStep> testStepList, String taskNum) {

        int rowNum = 16;
        Sheet sheet = workbook.getSheetAt(0);
        for (TestStep ts : testStepList) {
            Row row = sheet.createRow(rowNum++);
            createCell(0, rowNum, rowNum, row, sheet, ts.getName(), normalFontWhiteBackCenterSmall, false, 0);
            createCell(1, rowNum, rowNum, row, sheet, "Sikeres futás", normalFontWhiteBackCenterSmall, false, 1);
            createCell(2, rowNum, rowNum, row, sheet, "✔", normalFontWhiteBackCenterSmall, false, 2);
            createCell(3, rowNum, rowNum, row, sheet, getAssertionString(ts.getAssertions()), normalFontWhiteBackCenterSmall, false, 3);
        }

        writeTofile(taskNum);
    }

    private String getAssertionString(List<Assertion> assertionList) {
        String cellValue = "";
        CreationHelper createHelper = workbook.getCreationHelper();
        if (assertionList.size() == 0) {
            cellValue = "";
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("Assertionök:");
            for (Assertion assertion : assertionList) {
                sb.append("\n").append(assertion.getName()).append(" - ").append(assertion.getType());
            }
            cellValue = createHelper.createRichTextString(sb.toString().replace("type", "Típus").replace("name", "Neve")).toString();
        }
        return cellValue;
    }

    private void exportToXlsxPOTENYR(List<TestStep> testStepList, String taskNum, String reqID) {


        CreationHelper createHelper = workbook.getCreationHelper();
        prepareTheWorkbookPOTENYR();

        int stepNum = 1;
        Sheet sheet = workbook.getSheetAt(0);
        for (TestStep ts : testStepList) {
            Row row = sheet.createRow(stepNum++);
            Cell cell = row.createCell(0);
            cell.setCellValue(stepNum - 1);

            cell = row.createCell(1);
            cell.setCellValue("POT/ENYR");

            cell = row.createCell(2);
            cell.setCellValue(ts.getName());

            cell = row.createCell(3);
            cell.setCellValue("Sikeres futás.");

            cell = row.createCell(4);
            cell.setCellValue("Sikeres futás.");

            cell = row.createCell(7);
            cell.setCellValue("OK");
            cell.setCellStyle(greenFontGreenBackCenterAlign);

            cell = row.createCell(8);

            if (ts.getAssertions().size() == 0) {
                cell.setCellValue("");
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("Assertionök:");
                for (Assertion assertion : ts.getAssertions()) {
                    sb.append("\n").append(assertion.getName()).append(" - ").append(assertion.getType());
                }
                cell.setCellValue(createHelper.createRichTextString(sb.toString().replace("type", "Típus").replace("name", "Neve")));
            }
        }
        try {
            FileOutputStream fileOut = new FileOutputStream("C:/Kun/" + fileNameGeneratorPOTENYR(taskNum, reqID));
            workbook.write(fileOut);
            workbook.close();
            fileOut.close();

            System.out.println("Excel generálás megtörtént! Elérési út:");
        } catch (Exception ex) {
            System.out.println("Hiba: " + ex.getMessage());
        }

    }

    private String fileNameGeneratorPOTENYR(String taskNum, String reqID) {
        return "NPOT-" + taskNum + "_POT-" + reqID + "_TN.xlsx";
    }

    private String fileNameGeneratorTAKINFO(String taskNum) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
        Date date = new Date();
        dateFormat.format(date); //2016_11_16
        return "Tesztjegyzőkönyv_" + dateFormat.format(date) + "_" + taskNum + ".xlsx";
    }

    private void writeTofile(String taskNum) {
        try {
            FileOutputStream fileOut = new FileOutputStream("C:/Kun/" + fileNameGeneratorTAKINFO(taskNum));
            workbook.write(fileOut);
            workbook.close();
            fileOut.close();

            System.out.println("Excel generálás megtörtént! Elérési út:");
        } catch (Exception ex) {
            System.out.println("Hiba: " + ex.getMessage());
        }
    }

    private short pixel2WidthUnits(int pxs) {
        short widthUnits = (short) (EXCEL_COLUMN_WIDTH_FACTOR * (pxs / UNIT_OFFSET_LENGTH));
        widthUnits += UNIT_OFFSET_MAP[(pxs % UNIT_OFFSET_LENGTH)];
        return widthUnits;
    }

    private String projectSwitch(String projectCode) {
        switch (projectCode) {
            case "1":
                return "POT/ENYR";
            case "2":
                return "EESZT";
            case "3":
                return "TAKINFO";
        }
        return "";
    }
}
