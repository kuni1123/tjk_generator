package com.tjkwriter.soapui;

import com.tjkwriter.soapui.processor.XmlToExcelProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@SpringBootApplication
public class TjkApplication {

    public static void main(String[] args) {
        XmlToExcelProcessor xmlToExcelProcessor = new XmlToExcelProcessor();
        SpringApplication.run(TjkApplication.class, args);
        boolean mehet = false;
        String projectCode = "";
        String taskNum = "";
        String reqID = "";
        Scanner scanner;
        scanner = new Scanner(System.in);
        String filePath = "";
        try {
            projectCode = chooseProject();
        } catch (IOException io) {
            System.out.println("Hiba: " + io.getMessage());
        }
        while (!mehet) {
            try {
                System.out.println("Add meg az XML fájl elérési útját!");
                filePath = scanner.nextLine();
                BufferedReader br = new BufferedReader(new FileReader(filePath));

                if (projectCode.equals("1")) {
                    System.out.println("Add meg a feladat számát:");
                    taskNum = scanner.nextLine();
                    System.out.println("Add meg a POT számát:");
                    reqID = scanner.nextLine();
                    if (br.readLine() != null && taskNum != null && reqID != null) {
                        mehet = true;
                    }
                } else if (projectCode.equals("3") ||projectCode.equals("2")) {
                    System.out.println("Add meg a feladat számát:");
                    taskNum = scanner.nextLine();
                    if (br.readLine() != null && taskNum != null) {
                        mehet = true;
                    }
                }

            } catch (IOException io) {
                System.out.println("Hiba! " + io.getMessage());
            }
        }
        xmlToExcelProcessor.parseXML(filePath, taskNum, reqID, projectCode);
        System.out.println("Sikeres feldolgozás!");
    }

    private static String chooseProject() throws IOException {

        System.out.println("Melyik projekt?\n1. POT/ENYR\n2. EESZT\n3. TAKINFO");
        int projectCode = 0;

        List<Integer> lehetseges = new ArrayList<>();
        lehetseges.add(1);
        lehetseges.add(2);
        lehetseges.add(3);

        while (!lehetseges.contains(projectCode)) {
            Scanner scanner;
            scanner = new Scanner(System.in);
            projectCode = Integer.parseInt(scanner.nextLine());
        }

        return Integer.toString(projectCode);
    }

}
/*
c:/AFR-readyapi-project.xml
*/